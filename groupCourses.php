<?php

require('databaseInterface.php');

?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Group Courses</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<body>

<form id="searchForm">
    <fieldset>
        <legend>Course Search</legend>
        <div>
            <label for="degreeType">Degree</label>
            <select id="degreeType" name="degreeType" onchange="selectionMade()" size="2">
                <?php
                $categoriesResults = getCourseCategories();
                while($row = $categoriesResults->fetch())
                {
                    echo "<option value=" . str_replace(" ", "_", $row['category_name']) .
                        ">" . $row['category_name'] . "</option>";
                }
                ?>
            </select>
        </div>
        <div>
            <label for="catalogYear">Catalog Year</label>
            <select id="catalogYear" name="catalogYear" onchange="selectionMade()" size="2">
                <?php
                $categoriesResults = getCatalogYears();
                while($row = $categoriesResults->fetch())
                {
                    echo "<option value=" . str_replace(" ", "_", $row['catalog_year']) . ">"
                        . $row['catalog_year'] . "</option>";
                }
                ?>
            </select>
        </div>
    </fieldset>

</form>

<div id = "resultsBox">

</div>

</body>

<script>

    function removeUnderscores(str)
    {
        return str.replace(/_/g," ");
    }

    function selectionMade()
    {
        var degreeChoice = $("#degreeType").val() || [];
        var catalogYear = $("#catalogYear").val() || [];
        var options;

        if(degreeChoice.length > 0 && catalogYear.length > 0) {
            options = 'courseYear=' + catalogYear + '&degree=' + degreeChoice;

            $.ajax({
                datatype: "string",
                type: "GET",
                url: "./AJAXRequestHandler.php",
                data: options,
                success: function (result) {
                    $("#resultsBox").html("");
                    var courses = JSON.parse(result);
                    var year;
                    var courseID;
                    for (var i = 0; i < courses['courses'].length; i++) {
                        for (var x in courses['courses'][i]) {
                            //console.log(x);
                            // console.log(courses['courses'][i][x]);
                            switch (x) {
                                case "Course ID":
                                    courseID = courses['courses'][i][x];
                                    break;
                                case "Catalog Year":
                                    year = courses['courses'][i][x];
                                    break;
                                case "Course JSON":
                                    var course = courses['courses'][i][x];
                                    $("#resultsBox").append("<p>");
                                    for (var key in course) {
                                        //    console.log(key + ": " + course[key]);
                                        switch (key) {
                                            case "mainCourseNumber":
                                            case "mainCourseTitle":
                                                $("#resultsBox").append(" " + course[key] + " ");
                                                break;
                                            case  "mainCourseYear":
                                                $("#resultsBox").append(" (Year: " + course[key] + ") ");
                                                break;
                                        }
                                    }
                                    $("#resultsBox").append("</p>");
                                    break;
                            }

                        }
                        $("#resultsBox").append("<hr>");
                    }
                }

            }); // Ajax Call
        }
    }


</script>

</html>