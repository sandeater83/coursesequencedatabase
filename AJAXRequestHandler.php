<?php
/**
 * Created by PhpStorm.
 * User: stuartadams
 * Date: 4/16/15
 * Time: 12:28 PM
 */

include('databaseInterface.php');
print_r($_REQUEST);

echo "<br>";

if(isset($_REQUEST['electives']))
{
    $emphasisCats = '{"emphasis":[';
    $results = getElectiveCategories();

    $categories = $results->fetchAll();

    foreach($categories as $category)
    {
        $emphasisCats = $emphasisCats . '"' . $category[0] . '",';
    }

    $emphasisCats = rtrim($emphasisCats, ",");
    $emphasisCats = $emphasisCats . ']}';
    ob_clean();
    print $emphasisCats;
}

if(isset($_REQUEST['courseYear']) || isset($_REQUEST['degree']))
{
    $courses = '{"courses":[{';

    $year = str_replace("_", " ", $_REQUEST['courseYear']);
    $degree = str_replace("_", " ", $_REQUEST['degree']);

    if(isset($_REQUEST['courseYear']) && isset($_REQUEST['degree']))
    {
        $results = getCourseByYearAndDegree($year,$degree)->fetchAll();
    }
    elseif(isset($_REQUEST['courseYear']))
    {
        $results = getCourseByYear($year)->fetchAll();
    }
    elseif(isset($_REQUEST['degree']))
    {
        $results = getCourseByDegree($degree)->fetchAll();
    }



    foreach ($results as $result)
    {
        $courses = $courses . '"Course ID":"' . $result[0] . '","Catalog Year":"' . $result[1] . '","Course JSON":'
        . $result[2] . '},{';
    }

    $courses = rtrim($courses, ",{");
    $courses = $courses . ']}';
    ob_clean();
    print $courses;
}

?>