<?php
/**
 * Created by PhpStorm.
 * User: stuartadams
 * Date: 1/29/15
 * Time: 11:49 AM
 */

$didConnect = false;

function openConnection()
{
    $dsn = 'mysql:host=137.190.19.20;port=54896;dbname=W01193251';
    $username = 'W01193251';
    $password = 'Stuartcs!';


    try {
        $db = new PDO($dsn, $username, $password);
        $GLOBALS['didConnect'] = true;
        return $db;
    } catch (PDOException $e) {
        $error_message = $e->getMessage();
        exit();
    }

}

function postNewCategory($category)
{

    $bInDatabase = false;

    try
    {
        $results = getCourseCategories();

        while($row = $results->fetch())
        {
            if(in_array($category, $row))
            {
                $bInDatabase = true;
            }
        }

        if(!$bInDatabase)
        {
            $db = openConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO courseCategories (category_name) VALUE ('$category')";
            $db->exec($sql);
        }

    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
    $db = null;

}

function postElectiveEmphasisCategory($category)
{
    $bInDatabase = false;

    try
    {
        $results = findElectiveCategory($category)->fetchAll();

        if(count($results) > 0)
        {
            $bInDatabase = true;
        }

        if(!$bInDatabase)
        {
            $db = openConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO csElectiveEmphasisCategories (emphasis_name) VALUE ('$category')";
            $db->exec($sql);
        }

    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
    $db = null;
}

function findElectiveCategory($category)
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT emphasis_name
                FROM csElectiveEmphasisCategories
                WHERE emphasis_name = '$category'";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getElectiveCategories()
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT emphasis_name
                FROM csElectiveEmphasisCategories";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getCourseCategories()
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT category_name
                FROM courseCategories";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getCourseCategoryID($categoryName)
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT category_id
                FROM courseCategories
                WHERE category_name = '$categoryName'";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getCourseByYearAndNumber($courseYear, $courseNumber)
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT *
                FROM courses
                WHERE course_number = '$courseNumber' AND catalog_year = '$courseYear'";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getCourseByYear($courseYear)
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT *
                FROM courses
                WHERE catalog_year = '$courseYear'";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getCourseByDegree($degree)
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT *
                    FROM courses
                    INNER JOIN degreeCourses
                    ON courses.course_ID = degreeCourses.course_ID
                    WHERE categories_ID = (SELECT category_id
                                           FROM courseCategories
                                           WHERE category_name = '$degree')";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getCourseByYearAndDegree($year, $degree)
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT *
                    FROM courses
                    INNER JOIN degreeCourses
                    ON courses.course_ID = degreeCourses.course_ID
                    WHERE catalog_year = '$year' AND categories_ID = (SELECT category_id
                                           FROM courseCategories
                                           WHERE category_name = '$degree')";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function getDegreeCourse($courseID, $courseYear, $categoryID)
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT *
                FROM degreeCourses
                WHERE course_year = '$courseYear' AND course_ID = '$courseID' AND categories_ID = '$categoryID'";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }
    $db = null;
}

function postDegreeCourse($courseID, $courseYear, $categoryID)
{
    $bInDatabase = false;

    try
    {
        $results = getDegreeCourse($courseID, $courseYear, $categoryID);

        if(count($results->fetchAll()) > 0)
        {
            $bInDatabase = true;
        }

        if(!$bInDatabase)
        {
            $db = openConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO degreeCourses (course_ID, course_year, categories_ID)
                    VALUE ('$courseID', '$courseYear', '$categoryID')";
            $db->exec($sql);
        }
        $db = null;

    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}

function postCourse($catalogYear, $courseNum, $courseJSONObject)
{
    $bInDatabase = false;

    try
    {
        $results = getCourseByYearAndNumber($catalogYear, $courseNum);

        if(count($results->fetchAll()) > 0)
        {
            $bInDatabase = true;
        }

        if(!$bInDatabase)
        {
            $db = openConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO courses (catalog_year, course_number, course_JSON_object)
                    VALUE ('$catalogYear', '$courseNum', '$courseJSONObject')";
            $db->exec($sql);
        }

    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
    $db = null;
}

function postData($inputData)
{
    if(!isset($inputData['mainCourseNumber']) && !isset($inputData['mainCourseTitle'])
    && !isset($inputData['mainCourseYear']) && !isset($inputData['catalogUrl'])
        && !isset($inputData['degreeType']))
    {
        return;
    }
    $db = openConnection();
    $aDegreeType = $inputData['degreeType'];
    $aEmphasisCategories = $inputData['emphasisCategories'];
    $categoryIDs = array();

    print_r($inputData);
    echo "<br>";
    for($i = 0; $i < sizeOf($aDegreeType); $i++)
    {
        $aDegreeType[$i] = str_replace("_", " ", $aDegreeType[$i]);
        if($aDegreeType[$i] == "Other")
        {
            $aDegreeType[$i] = $inputData['otherDegreeCategory'];
            unset($inputData['otherDegreeCategory']);
            postNewCategory($aDegreeType[$i]);
        }

        $categoryID = getCourseCategoryID($aDegreeType[$i])->fetch();
        array_push($categoryIDs, $categoryID[0]);
    }

    for($i = 0; $i < sizeOf($aEmphasisCategories); $i++)
    {
        $aEmphasisCategories[$i] = str_replace("_", " ", $aEmphasisCategories[$i]);
        if($aEmphasisCategories[$i] == "Other")
        {
            $aEmphasisCategories[$i] = $inputData['otherEmphasisCategory'];
            unset($inputData['otherEmphasisCategory']);
            postElectiveEmphasisCategory($aEmphasisCategories[$i]);
        }
    }

    $inputData['degreeType'] = $aDegreeType;
    $inputData['emphasisCategories'] = $aEmphasisCategories;
    $jsonData = json_encode($inputData);

    foreach($categoryIDs as $id)
    {
        postCourse($inputData['mainCourseYear'], $inputData['mainCourseNumber'], $jsonData);
        $courseID = getCourseByYearAndNumber($inputData['mainCourseYear'], $inputData['mainCourseNumber'])->fetch();
        postDegreeCourse($courseID['course_ID'], $inputData['mainCourseYear'], $id);
    }
    echo $jsonData;

    $db = null;
}

function getCatalogYears()
{
    $db = openConnection();

    try
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT DISTINCT catalog_year
                FROM courses";
        $results = $db->query($query);
        return $results;
    }
    catch(PDOException $e)
    {
        print_r($e);
    }

    $db = null;
}
?>