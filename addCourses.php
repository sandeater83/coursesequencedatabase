<?php
/**
 * Created by PhpStorm.
 * User: stuartadams
 * Date: 4/7/15
 * Time: 10:46 AM
 */
require('databaseInterface.php');
if(!empty($_REQUEST))
{


    postData($_REQUEST);

}

getElectiveCategories();

?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Add Courses</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<body>

<form action="addCourses.php" method="post">
    <fieldset>
        <legend>Course Information</legend>
        <div>
            <label for="mainCourseNumber">Course Number:</label>
            <input type="text" id="mainCourseNumber" name="mainCourseNumber" required>
        </div>
        <div>
            <label for="mainCourseTitle">Course Name:</label>
            <input type="text" id="mainCourseTitle" name="mainCourseTitle" required>
        </div>
        <div>
            <label for="mainCourseYear">Course Catalog Year:</label>
            <input type="number" id="mainCourseYear" name="mainCourseYear" required>
        </div>
        <div>
            <label for="catalogUrl">Catalog Course URL:</label>
            <input type="text" id="catalogUrl" name="catalogUrl" required>
        </div>
        <div>
            <label for="degreeType">For Degree, Elective or Certificate</label>
            <select id="degreeType" multiple name="degreeType[]" required onchange="madeDegreeSelection()" size="2">
                <?php
                    $categoriesResults = getCourseCategories();
                    while($row = $categoriesResults->fetch())
                    {
                        echo "<option value=" . str_replace(" ", "_", $row['category_name']) . ">" . $row['category_name'] . "</option>";
                    }
                ?>
                <option value="Other">Other...</option>
            </select>
            <div id="degreeCategories">

            </div>
        </div>
        <div>
            <div id="CSEmphasisSelection">

            </div>
            <div id="CSEmphasisOther">

            </div>
        </div>
    </fieldset>
        <fieldset>
            <legend>Prerequisites</legend>
            <div id="prerequisites">
                <script>
                    $(document).ready(function(){
                        addPreq();
                    });
                </script>
            </div>
            <input type="button" value="Add Prerequisite" onclick="addPreq()">

        </fieldset>

        <fieldset>
            <legend>Required For:</legend>
            <div id="requiredFor">

            </div>
            <input type="button" value="Add Required For Classes" onclick="addRequiredFor()">
        </fieldset>


        <input type="submit">


</form>

</body>

<script>
    var prereqCount = 0;
    var requiredForCount = 0;

    function addPreq()
    {
        var idString = "PreReq" + prereqCount;
        var nameString = "PreReq[]";
        $("#prerequisites").append('<div> <label for="'+ idString +'">Course Number and Name:</label>' +
        '<input type="text" id="' + idString + '" name="' + nameString +'" placeholder="Math 1210 - Calculus">'+
        '</div>');

        prereqCount++;
    }

    function addRequiredFor()
    {
        var idString = "Required" + requiredForCount;
        var nameString = "Required[]";
        var checkboxString = "takeConcurrent_" + requiredForCount;
        $("#requiredFor").append('<div> <label for="' + idString + '">Course Name:</label>' +
        '<input type="text" id="' + idString + '" name="' + nameString + '">' +
        '<label for="' + checkboxString + '">May take concurrently?</label>' +
        '<input type="checkbox" id="' + checkboxString + '" name="' + checkboxString +'">' + '</div>');

        requiredForCount++;
    }

    function madeDegreeSelection()
    {
        var degreeChoice = $("#degreeType").val() || [];

        for(var item in degreeChoice)
        {
            if(degreeChoice[item] == "Other")
            {
                $("#degreeCategories").append('<label id="labelForOtherCategories" for="otherDegreeCategory">Enter category name:</label>' +
                '<input type="text" id="otherDegreeCategory" name="otherDegreeCategory" required placeholder="Some New Certificate">');
            }
            else
            {
                $("#labelForOtherCategories").remove();
                $("#otherDegreeCategory").remove();
            }

            if(degreeChoice[item] == "CS_Electives")
            {
                getEmphasisOptions();
            }
            else
            {
                $("#emphasisCategory").remove();
                $("#emphasisSelectLbl").remove();
            }
        }
    }

    function madeEmphasisSelection()
    {
        var choice = $("#emphasisCategory").val() || [];

        for(var item in choice)
        {
            if(choice[item] == "Other")
            {
                $("#CSEmphasisOther").append('<label id="labelForOtherEmphasisCategories" for="otherEmphasisCategory">Enter emphasis category name:</label>' +
                '<input type="text" id="otherEmphasisCategory" name="otherEmphasisCategory" required placeholder="Some New Elective Emphasis">');
            }
            else
            {
                $("#labelForOtherEmphasisCategories").remove();
                $("#otherEmphasisCategory").remove();
            }

        }
    }

    function getEmphasisOptions()
    {
        var optionList;
        console.log("hello");
        $.ajax({

            datatype: "string",
            type: "GET",
            url: "./AJAXRequestHandler.php",
            data: 'electives=yes',
            success: function (result) {
                optionList = JSON.parse(result);

                $("#CSEmphasisSelection").append('<label id="emphasisSelectLbl" for="emphasisCategory">Elective Emphasis Category</label>' +
                '<select id="emphasisCategory" multiple="" name="emphasisCategories[]" required onchange="madeEmphasisSelection()" size="2">' +
                '</select>');

                for (var x = 0; x < optionList['emphasis'].length; x++) {
                    var value = optionList['emphasis'][x].replace(/ /g,"_");
                    $("#emphasisCategory").append('<option value="' + value +'">' + optionList['emphasis'][x] +'</option>');
                }
                $("#emphasisCategory").append('<option value="Other">Other...</option>');
            }

        }); // Ajax Call
    }

</script>

</html>